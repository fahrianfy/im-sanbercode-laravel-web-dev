
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>String PHP</title>
</head>
<body>
    <h1>Berlatih String PHP</h1>
    <?php   
        echo "<h3> Soal No 1</h3>";
        $first_sentence = "Hello PHP!" ; 
        echo "Panjang string: " . strlen($first_sentence) . ", jumlah kata: " . str_word_count($first_sentence) . "<br>";
        $second_sentence = "I'm ready for the challenges"; 
        echo "Panjang string: " . strlen($second_sentence) . ", jumlah kata: " . str_word_count($second_sentence) . "<br>";
        
        echo "<h3> Soal No 2</h3>";
        $string2 = "I love PHP";
        echo "<label>String: </label> $string2 <br>";
        echo "Kata pertama: " . explode(" ", $string2)[0] . "<br>"; 
        echo "Kata kedua: " . explode(" ", $string2)[1] . "<br>"; 
        echo "Kata Ketiga: " . explode(" ", $string2)[2] . "<br>";

        echo "<h3> Soal No 3 </h3>";
        $string3 = "PHP is old but sexy!";
        echo "String: ".str_replace('sexy!','awesome', $string3).""; 

    ?>
</body>
</html> 
