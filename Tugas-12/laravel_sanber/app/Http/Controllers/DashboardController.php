<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        return view('home');
    }

    public function register()
    {
        return view('register');
    }

    public function welcome(Request $request)
    {
    $validatedData = $request->validate([
        'first_name' => 'required',
        'last_name' => 'required',
    ]);
    return view('welcome', [
        'first_name' => $validatedData['first_name'],
        'last_name' => $validatedData['last_name']
    ]);
    }
}
