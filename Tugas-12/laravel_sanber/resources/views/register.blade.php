<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SanberBook - Register</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>

    <form action="{{ route('welcome') }}" method="POST">
        @csrf

        <label for="first_name">First name:</label>
        <input type="text" id="first_name" name="first_name"><br><br>

        <label for="last_name">Last name:</label>
        <input type="text" id="last_name" name="last_name"><br><br>

        <label>Gender:</label><br>
        <input type="radio" id="male" name="gender" value="male">
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="gender" value="female">
        <label for="female">Female</label><br>
        <input type="radio" id="other" name="gender" value="other">
        <label for="other">Other</label><br><br>

        <label for="nationality">Nationality:</label>
        <select id="nationality" name="nationality">
            <option value="indonesian">Indonesian</option>
            <!-- Tambahkan opsi lain jika diperlukan -->
        </select><br><br>

        <label>Language Spoken:</label><br>
        <input type="checkbox" id="bahasa" name="language[]" value="bahasa">
        <label for="bahasa">Bahasa Indonesia</label><br>
        <input type="checkbox" id="english" name="language[]" value="english">
        <label for="english">English</label><br>
        <input type="checkbox" id="other_lang" name="language[]" value="other">
        <label for="other_lang">Other</label><br><br>

        <label for="bio">Bio:</label><br>
        <textarea id="bio" name="bio"></textarea><br><br>

        <input type="submit" value="Sign Up">
    </form>
</body>
</html>
