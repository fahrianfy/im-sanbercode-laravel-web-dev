<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('film_cast', function (Blueprint $table) {
            $table->string('name', 45)->after('cast_id')->nullable(); // Menambahkan kolom 'name' setelah kolom 'cast_id'
        });
    }

    public function down()
    {
        Schema::table('film_cast', function (Blueprint $table) {
            $table->dropColumn('name'); // Menghapus kolom 'name' jika melakukan rollback
        });
    }
};
