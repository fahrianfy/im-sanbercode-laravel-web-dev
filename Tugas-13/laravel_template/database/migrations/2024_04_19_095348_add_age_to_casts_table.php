<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('casts', function (Blueprint $table) {
            $table->integer('umur')->after('bio')->nullable(); // Menambahkan kolom 'umur' setelah kolom 'bio'
        });
    }

    public function down()
    {
        Schema::table('casts', function (Blueprint $table) {
            $table->dropColumn('umur'); // Menghapus kolom 'umur' jika melakukan rollback
        });
    }
};
