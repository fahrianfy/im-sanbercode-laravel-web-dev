@extends('layouts.master')

@section('title', 'View Cast Member')

@section('content_header')
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">View Cast Member</h1>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="form-group">
                            <label>Name:</label>
                            <input type="text" class="form-control" value="{{ $cast->name }}" readonly>
                        </div>
                        <div class="form-group">
                            <label>Age:</label>
                            <input type="number" class="form-control" value="{{ $cast->umur }}" readonly>
                        </div>
                        <div class="form-group">
                            <label>Bio:</label>
                            <textarea class="form-control" readonly>{{ $cast->bio }}</textarea>
                        </div>
                        <a href="{{ route('cast.index') }}" class="btn btn-default">Back to List</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
