@extends('layouts.master')

@section('title', 'Cast Members')

@section('content_header')
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0">Cast Members</h1>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <!-- Default box -->
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">List of Cast Members</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="castsTable" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Age</th>
                                <th>Bio</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($casts as $cast)
                                <tr>
                                    <td>{{ $cast->name }}</td>
                                    <td>{{ $cast->umur }}</td>
                                    <td>{{ $cast->bio }}</td>
                                    <td>
                                        <a href="{{ route('cast.show', $cast->id) }}" class="btn btn-sm btn-success">View</a>
                                        <a href="{{ route('cast.edit', $cast->id) }}" class="btn btn-sm btn-info">Edit</a>
                                        <form action="{{ route('cast.destroy', $cast->id) }}" method="POST" style="display: inline-block;">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-sm btn-danger" onclick="return confirm('Are you sure?')">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>Name</th>
                                <th>Age</th>
                                <th>Bio</th>
                                <th>Actions</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>
</section>
@endsection

@push('css')
    <link rel="stylesheet" href="{{ asset('adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
@endpush

@push('scripts')
    <script src="{{ asset('adminlte/plugins/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('#castsTable').DataTable({
                "responsive": true,
                "lengthChange": true,
                "autoWidth": false
            });
        });
    </script>
@endpush