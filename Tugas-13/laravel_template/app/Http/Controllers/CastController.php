<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    // Menampilkan list semua casts
    public function index()
    {
        $casts = DB::table('casts')->get();
        return view('casts.index', ['casts' => $casts]);
    }

    // Menampilkan form untuk membuat cast baru
    public function create()
    {
        return view('casts.create');
    }

    // Menyimpan cast baru ke database
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:45',
            'umur' => 'nullable|integer',
            'bio' => 'nullable'
        ]);

        DB::table('casts')->insert([
            'name' => $validatedData['name'],
            'umur' => $validatedData['umur'],
            'bio' => $validatedData['bio']
        ]);

        return redirect()->route('cast.index');
    }

    // Menampilkan detail cast berdasarkan ID
    public function show($cast_id)
    {
        $cast = DB::table('casts')->where('id', $cast_id)->first();
        return view('casts.show', ['cast' => $cast]);
    }

    // Menampilkan form untuk mengedit cast
    public function edit($cast_id)
    {
        $cast = DB::table('casts')->where('id', $cast_id)->first();
        return view('casts.edit', ['cast' => $cast]);
    }

    // Memperbarui data cast di database
    public function update(Request $request, $cast_id)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:45',
            'umur' => 'nullable|integer',
            'bio' => 'nullable'
        ]);

        DB::table('casts')->where('id', $cast_id)->update([
            'name' => $validatedData['name'],
            'umur' => $validatedData['umur'],
            'bio' => $validatedData['bio']
        ]);

        return redirect()->route('cast.index');
    }

    // Menghapus cast berdasarkan ID
    public function destroy($cast_id)
    {
        DB::table('casts')->where('id', $cast_id)->delete();
        return redirect()->route('cast.index');
    }
}
