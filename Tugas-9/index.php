<?php
require_once("animal.php");
require_once("Ape.php");
require_once("Frog.php");

// Instantiate the Animal class
$sheep = new Animal("shaun");
echo "Name : " . $sheep->name . "<br>"; // "shaun"
echo "legs : " . $sheep->legs . "<br>"; // 4
echo "cold blooded : " . $sheep->cold_blooded . "<br><br>"; // "no"

// Instantiate the Ape class
$sungokong = new Ape("kera sakti");
echo "Name : " . $sungokong->name . "<br>"; // "kera sakti"
echo "legs : " . $sungokong->legs . "<br>"; // 2
echo "cold blooded : " . $sungokong->cold_blooded . "<br>";
echo "Yell : "; $sungokong->yell(); // "Auooo"
echo "<br><br>";

// Instantiate the Frog class
$kodok = new Frog("buduk");
echo "Name : " . $kodok->name . "<br>"; // "buduk"
echo "legs : " . $kodok->legs . "<br>"; // 4
echo "cold blooded : " . $kodok->cold_blooded . "<br>";
echo "Jump : "; $kodok->jump(); // "hop hop"
?>
